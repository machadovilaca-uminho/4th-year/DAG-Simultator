from random import randrange
from typing import Dict, List

from networkx import nx
from simulator.dag.graph_generator import generate_dag_graph
from simulator.messages.request_message import RequestMessage

from simulator.messages.response_message import ResponseMessage
from simulator.operations.sum_operation import SumOperation
from simulator.statistics.chart import draw_error_loss, draw_error_nodes

state = {}


def update_node_parents(sender_node: int, target_node: int, parents_ids: [int]):
    if target_node not in state["nodes_grandparents"]:
        state["nodes_grandparents"][target_node] = {}

    node_grandparents = state["nodes_grandparents"][target_node]

    if sender_node not in node_grandparents and sender_node is not None:
        node_grandparents[sender_node] = parents_ids


def update_preferred(sender_node: int, node: int):
    gps: Dict[int, List[int]] = {}

    for parent in state["nodes_grandparents"][node]:
        for gp in state["nodes_grandparents"][node][parent]:
            if gp not in gps:
                gps[gp] = [parent]
            else:
                gps[gp].append(parent)

    mcp = (None, 0)

    for gp in gps:
        if len(gps[gp]) > mcp[1]:
            mcp = (gp, len(gps[gp]))

    if mcp[0] is not None:
        state["preferred_grandparents"][node] = (mcp[0], gps[mcp[0]])
    else:
        state["preferred_grandparents"][node] = (None, [sender_node])


def return_response(response, receiver_node):
    state["sent"] += 1

    nr = 0 if state["lost_percentage"] == 0 else round(1 / state["lost_percentage"] * 100)

    if nr != 0 and randrange(nr) % nr == 0:
        state["lost"] += 1
        return

    if receiver_node == response.target_node_id:
        for res in state["results"]:
            if res.r_id == response.r_id:
                return

        state["results"].append(response)

        return

    for n in state["nodes_grandparents"][receiver_node]:
        return_response(response, n)


def return_answer(message):
    operation = message.operation
    result = operation.aggregate([operation]).result

    state["r_id"] += 1

    if message.target_node_id in state["nodes_grandparents"]:
        for n in state["nodes_grandparents"][message.target_node_id]:
            response = ResponseMessage(message.m_id, state["r_id"], message.target_node_id, state["root_node"], result)
            return_response(response, n)


def forward(message, current_node, neighbors):
    operation = message.operation
    operations = operation.split(operation, len(neighbors))

    for i in range(0, len(operations)):
        state["m_id"] = state["m_id"] + 1
        new_message = RequestMessage(
            state["m_id"], current_node, list(nx.ancestors(state["graph"], current_node)), neighbors[i], operations[i]
        )

        broadcast(new_message)


def broadcast(message: RequestMessage):
    sender_node = message.sender_node_id
    target_node = message.target_node_id
    parents_ids = message.sender_node_parents_id
    operation = message.operation

    if sender_node is not None:
        update_node_parents(sender_node, target_node, parents_ids)
        update_preferred(sender_node, target_node)

    neighbors = list(nx.descendants(state["graph"], target_node))

    if not operation.splittable(operation, len(neighbors)):
        return_answer(message)
    else:
        forward(message, target_node, neighbors)


def run(n_nodes, n_sum_list, n_lost_percentage):
    state["r_id"] = 0
    state["preferred_grandparents"] = {}
    state["nodes_grandparents"] = {}
    state["sent"] = 0
    state["lost"] = 0
    state["results"] = []
    state["nodes"] = n_nodes
    state["lost_percentage"] = n_lost_percentage
    state["graph"] = generate_dag_graph(state["nodes"], n_lost_percentage)

    # nx.draw(graph, with_labels=True)
    # plt.show()

    state["root_node"] = list(nx.topological_sort(state["graph"]))[0]

    state["m_id"] = 1

    message = RequestMessage(state["m_id"], None, [], state["root_node"], SumOperation(n_sum_list, 0))
    broadcast(message)

    r = 0

    for res in state["results"]:
        r += res.result

    return sum(n_sum_list), r, state["lost"], state["sent"]


if __name__ == "__main__":
    iterations = 5
    n_nodes = 7
    n_sum_list = list(range(1, 100))
    n_lost_percentage = 0

    j = []
    k = []
    x = []
    y = []
    z = []

    for i in range(0, iterations):
        expected, given, lost, sent = run(n_nodes, n_sum_list, n_lost_percentage)
        n_lost_percentage += 10
        j.append(n_nodes)
        k.append(expected - given)

        x.append(expected)
        y.append(given)
        z.append(lost/sent * 100)

    draw_error_loss(x, y, z)
    #draw_error_nodes(j, k)
