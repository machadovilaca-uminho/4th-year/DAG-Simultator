import matplotlib.pyplot as plt


def draw_error_nodes(x, y1):
    fig, ax = plt.subplots(1, 1)
    ax.plot(x, y1)
    ax.set_xlabel('Número de nodos do grafo')
    ax.set_ylabel('Diferença entre resultado esperado e obtido')
    ax.grid(True)

    fig.tight_layout()
    plt.show()


def draw_error_loss(y1, y2, y3):
    x = range(0, len(y1))

    fig, ax1 = plt.subplots()

    ax1.plot(x, y3, color="tab:red")
    ax1.set_ylabel("Percentagem de mensagens perdidas")

    ax2 = ax1.twinx()

    ax2.plot(x, y1)
    ax2.plot(x, y2)
    ax2.fill_between(x, y1, y2, alpha=0.5)
    ax2.set_ylabel("Área da diferença entre resultado obtido e o esperado")

    ax2.set_ylim([0, 5000])

    fig.tight_layout()
    plt.show()
