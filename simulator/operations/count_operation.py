import numpy as np


class CountOperation:
    result: int
    values: [int]

    def __init__(self, values: [int], result: int):
        self.result = result
        self.values = values

    def count(self):
        for _ in self.values:
            self.result += 1

    def splittable(self, operation, divisions: int):
        values_size = len(operation.values)

        return divisions > 0 and values_size > 2

    def split(self, operation, divisions: int):
        values_size = len(operation.values)

        operation.values[0] += operation.values.pop(values_size-1)
        values_size -= 1

        if divisions > values_size:
            divisions = values_size

        splits = np.array_split(operation.values, divisions)

        results = []

        for split in splits:
            results.append(CountOperation(list(split), operation.result))

        return results

    def aggregate(self, operations):
        final = 0

        for operation in operations:
            operation.count()

            final += operation.result

        return CountOperation([], final)
