import math

import networkx as nx
import matplotlib.pyplot as plt
import numpy as np


def calculate_losing_messages_probability(m, network_lose_percentage: int):
    n = len(m)

    for k in range(1, n):
        cs = 0

        for i in range(0, n):
            if m[i][k] == 1:
                cs += 1

        p = math.pow((network_lose_percentage / 100), cs)
        #print("P({}): {}".format(k, p))


def create_matrix(number_of_nodes: int, network_lose_percentage: int):
    m = np.zeros((number_of_nodes, number_of_nodes), dtype=np.int32)

    shift = math.ceil(number_of_nodes/5) + round(network_lose_percentage / 10)

    for i in range(0, number_of_nodes):
        for k in range(i + 1, min(i + shift, number_of_nodes)):
            m[i][k] = 1

    #print(m)
    calculate_losing_messages_probability(m, network_lose_percentage)

    return m


def generate_dag_graph(number_of_nodes: int, network_lose_percentage: int):
    m = create_matrix(number_of_nodes, network_lose_percentage)

    g = nx.from_numpy_matrix(m, create_using=nx.MultiDiGraph)

    return g


if __name__ == "__main__":
    dag = generate_dag_graph(7, 50)

    nx.draw(dag, with_labels=True)
    plt.show()
