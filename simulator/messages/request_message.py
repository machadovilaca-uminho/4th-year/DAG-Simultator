from typing import Any


class RequestMessage:
    m_id: int
    sender_node_id: int
    sender_node_parents_id: [int]
    target_node_id: int
    operation: Any

    def __init__(self, m_id, sender_node_id, sender_node_parents_id, target_node_id, operation):
        self.m_id = m_id
        self.sender_node_id = sender_node_id
        self.sender_node_parents_id = sender_node_parents_id
        self.target_node_id = target_node_id
        self.operation = operation
