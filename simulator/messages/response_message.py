from typing import Any


class ResponseMessage:
    r_id: int
    m_id: int
    sender_node_id: int
    target_node_id: int
    result: Any

    def __init__(self, r_id, m_id, sender_node_id, target_node_id, result):
        self.r_id = r_id
        self.m_id = m_id
        self.sender_node_id = sender_node_id
        self.target_node_id = target_node_id
        self.result = result
